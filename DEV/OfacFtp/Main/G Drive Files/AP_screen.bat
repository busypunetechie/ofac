echo off
rem B.Christyono/FHLBSF 04/26/2004
rem screening AP file ap_ofac.csv
rem +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
g:
cd \apsdata\OFAC\userfiles
if not exist ap_ofac.csv goto noextract
cd ..
start screen ap_format.pfi ap_ofac.csv
exit
:noextract
echo *** Error ******************************************************
echo extract file ap_ofac.csv not found
pause
