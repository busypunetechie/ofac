echo off
rem B.Christyono/FHLBSF 04/26/2004
rem screening Credit file CR_ofac.csv
rem HISTORY:
rem 01/16/07  BC   Changed to using MSAccess in C:\
rem 2/2015 CHG0031124 TWS - Revise for Window7 MSACCESS location.
rem +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
rem create extract first
set msaccess="C:\Program Files (x86)\Microsoft Office\Office12\MSAccess.exe"
%msaccess% g:\apsdata\ofac\CR_extract.mdb /x:export_customers
g:
cd \apsdata\OFAC\userfiles
if not exist CR_ofac.csv goto noextract
exit

:noextract
echo *** Error ******************************************************
echo extract file cr_ofac.csv not found
pause
