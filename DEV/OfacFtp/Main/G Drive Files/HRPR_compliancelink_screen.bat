echo off
rem B.Christyono/FHLBSF 04/26/2004
rem J.Richardson 22-Mar-2010. Switch over to ComplianceLink from FacFilter.
rem screening HR/PR file(s)
rem HR/PR files consist of pr_ofac.csv and prr_ofac.csv (retiree list). 
rem which will be merged before screened. 
rem +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
g:
cd \apsdata\OFAC\userfiles
if not exist pr_ofac.csv goto noextract
if exist prr_ofac.csv type prr_ofac.csv >> pr_ofac.csv
cd ..
rem start screen hr_format.pfi pr_ofac.csv
"c:\Program Files\Accuity\Compliance Link\CLcmd.exe" G:\APSDATA\OFAC\pr_compliancelink_screen.batch
echo CTRL-C to quit, or press RETURN to begin screening review/disposition
pause
"c:\Program Files\Accuity\Compliance Link\ComplianceLink.exe"
exit
:noextract
echo *** Error ******************************************************
echo extract file pr_ofac.csv not found
pause
