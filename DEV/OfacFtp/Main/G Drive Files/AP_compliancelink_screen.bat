echo off
rem B.Christyono/FHLBSF 04/26/2004
rem J.Richardson 22-Mar-2010. Switch over to ComplianceLink from FacFilter.
rem screening AP file ap_ofac.csv
rem +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
g:
cd \apsdata\OFAC\userfiles
if not exist ap_ofac.csv goto noextract
cd ..
rem start screen ap_format.pfi ap_ofac.csv
"c:\Program Files\Accuity\Compliance Link\CLcmd.exe" G:\APSDATA\OFAC\ap_compliancelink_screen.batch
echo CTRL-C to quit, or press RETURN to begin screening review/disposition
pause
"c:\Program Files\Accuity\Compliance Link\ComplianceLink.exe"
exit
:noextract
echo *** Error ******************************************************
echo extract file ap_ofac.csv not found
pause
