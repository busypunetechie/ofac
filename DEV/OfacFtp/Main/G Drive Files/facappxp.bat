echo off
rem update database first before screening
rem this assumes facapp is in the "d:\program files\tfp\FACFilter for CIF V2"
rem Heat 112604 12/2007 - Upgrade to Ver 3.0
set maindb="g:\apsdata\ofac\ffplus.dat"
set drv=c:
set loc="program files\tfp\FACFilter for CIF V3"
%drv%
cd \%loc%
if exist ffplus.dat del ffplus.dat
copy %maindb% .
start facapp

