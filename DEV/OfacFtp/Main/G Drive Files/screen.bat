echo off
rem B.Christyono/FHLBSF 04/26/2004
rem for screening extract and bring up FACFilter for dispositioning
rem HISTORY:
rem 3/29/05 BC  changed appdrv to C: since the app is in there on XP boxes
rem Requirements:
rem %1 = mapping file (*.pfi) to be stored in g:\apsdata\OFAC
rem %2 = extract file to be stored in g:\apsdata\OFAC\userfiles\
rem facapp is installed in "d:\program files\tfp\FACFilter for CIF V2"
rem ++++++++++++++++ define your spec/input file name here ++++++++++
set maindb="g:\apsdata\ofac\ffplus.dat"
set appdrv=C:
set apppath="program files\tfp\facfilter for cif v2"
set appname=facapp.exe
set specpath=g:\apsdata\OFAC
set extpath=g:\apsdata\OFAC\userfiles
set specname=%specpath%\%1
set extname=%extpath%\%2
set logfile=%extpath%\screen.log
rem +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if (%1) == () goto noparm
if (%2) == () goto noparm

if not exist %logfile% echo Creating log file > %logfile%
rem update database first before screening
%appdrv%
cd \%apppath%
if exist ffplus.dat del ffplus.dat
copy %maindb% .
if not exist %specname% goto invalid_spec
if not exist %extname% goto invalid_ext
echo *** Screening %extname% >> %logfile%
date/t >> %logfile%
time/t >> %logfile%
%appname% -pfi %specname% -filename %extname% -no vessels
echo CTRL-C to quit, or press RETURN to begin screening review/disposition
pause
start %appname%
echo *** Completed ************************************************** >> %logfile%
exit
:noparm
echo *** Error ******************************************************
echo *** Error, one or more parameter(s) unspecified >> %logfile%
date/t >> %logfile%
time/t >> %logfile%
echo parm1 %1, parm2 %2 >> %logfile%
echo one or more parameter(s) unspecified
echo Usage :  OFACScreen mapping_filename extract_filename
echo ****************************************************************
exit
:invalid_spec
echo *** Error ******************************************************
echo *** Error, invalid file specification >> %logfile%
date/t >> %logfile%
time/t >> %logfile%
echo parm1 %1, parm2 %2 >> %logfile%
echo Spec file %specname% could not be found
exit
:invalid_ext
echo *** Error ******************************************************
echo *** Error, invalid extract file >> %logfile%
date/t >> %logfile%
time/t >> %logfile%
echo parm1 %1, parm2 %2 >> %logfile%
echo Extract file %extname% could not be found
exit
