echo off
rem B.Christyono/FHLBSF 04/26/2004
rem screening HR/PR file(s)
rem HR/PR files consist of pr_ofac.csv and prr_ofac.csv (retiree list). 
rem which will be merged before screened. 
rem +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
g:
cd \apsdata\OFAC\userfiles
if not exist pr_ofac.csv goto noextract
if exist prr_ofac.csv type prr_ofac.csv >> pr_ofac.csv
cd ..
start screen hr_format.pfi pr_ofac.csv
exit
:noextract
echo *** Error ******************************************************
echo extract file pr_ofac.csv not found
pause
