
@echo off

rem  This batch job exists solely to run the FHLBSF.OfacFtp.exe, below.
rem  Appworx will kick off this job every day around 3:30pm. The exe, below,
rem  will connect to the Accuity ftp site to see if there's a new version
rem  of the Ofac file for us to download. I wrote FHLBSF.OfacFtp.exe so that
rem  by default it will download the MLGWLXML.ZIP file used by ComplianceLink.
rem  If you want, you can pass different parameters to the program that
rem  will override the defaults.

rem  The input parameters are tag-driven and the order is unimportant.
rem  The tag is not case-sensitive, but the *value* of the tag is important
rem  You can mix and match parameters as you wish. The list of parameters is this:

rem		filename:<the file name on the Accuity site>
rem		localzipfilename:<the local file name for the file downloaded>
rem		filepath:<the path where the file will be stored>
rem		ftpuserid:<Accuity ftp user id>
rem		ftppassword:<Accuity ftp password>
rem		ftpserver:<Accuity ftp server url>

rem J. Richardson. Dec, 2009.


echo Batch file param are: %1 %2 %3 %4 %5 %6

echo Batch Start %TIME%

\\fhlbsf\dfs\Apps\SchedulerUtil\FHLBSF.OfacFtp.exe %1 %2 %3 %4 %5 %6

echo Batch Done %time%

rem pause


