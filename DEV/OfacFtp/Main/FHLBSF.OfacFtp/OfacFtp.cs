﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Mail;


/*
 * OfacFtp
 * 
 * I wrote this originally because the Accuity gang have a new product called
 * ComplianceLink which replaces the old cif process. There are some similarities
 * to the old process in that we need to go to their FTP site and see if they
 * have updated bad-guy lists for us.
 * 
 * It would have been nice just to have plugged the new world into the old Appworx
 * BK_OFAC_FTP_CHAIN but it started to get ugly in a couple of places and I ended
 * up writing this. Problem #1, albeit trivial I suspect, is that the new ComplianceLink
 * client works off the actual zip file itself. The Appworx stream leaves the
 * original zip file in the target directory so unzipping the darn thing probably
 * doesn't matter.
 * 
 * Problem #2 is more annoying in that the new zip file name on the Accuity site is all
 * uppercase while the corresponding 'md5' checksum file name is all lower case. The
 * Appworx chain is assuming that all the names will be lowercase (it hardwires in
 * ".zip" and ".md5" as the file extensions). I just couldn't quite bring myself to
 * clone off the existing Appworx chain and so you have this. I tried to make this
 * general so you could, if you were so inclined, use this for all the Ofac downloads.
 * However, this code doesn't know about unzipping files so you'll have to figure out
 * how to deal with that ;-)
 * 
 * Same deal, sort of:
 * 
 *  1.  Pull down the md5 checksum file and compare it against the existing one. If
 *      they're the same, that means there's no new data on Accuity. We're done ...
 *      Send out an email to everybody saying that there are no changes.
 *      
 *  2.  If the checksums are different, pull down the zip file. Then pull down the md5
 *      file (again) but this time give it the name of the 'current' md5 file so the
 *      two md5 files will match. Then send out an email to the world telling them
 *      there's a new file.
 * 
 * 13-Jan-2010  JR  That didn't take long ... we were just about to go into production
 *              with this only to have Accuity change the file naming convention on
 *              us - specifically, the main mglgwlxml.zip file name is now lowercase.
 *              That must be their naming convention since all the other file names
 *              are in lower case. Oh well, it was more fun to write this program
 *              than to slog thru Appworx...
 *              
 */


namespace OfacFtp
{
    class OfacFtp
    {
        static void Main(string[] args)
        {
            bool allsWellThatEndsWell = true;
            try
            {
                Console.WriteLine("");
                Console.WriteLine("OfacFtp program start\n");

                OfacFtp ofacFtp = new OfacFtp();

                ofacFtp.help();

                /*
                 * We'll let the humans supply params if they feel the need to do
                 * so, but if they don't, we'll default to the ones below. These
                 * are for Accuity and the new MLGWLXML file.
                 */
                string zipFileName = "";
                string localZipFileName = "";
                string md5FileName = "";
                string localMd5FileName;
                string filePath = @"\\fhlbsf-i.com\dfs\Apps\Batchjobs\OFACFTP\CurrentDB";
                string ftpUserID = "fhlbsf01";
                string ftpPassword = "fhlbsf01";
                string ftpServerIP = "ftp.financialgo.net";

                zipFileName = "mlgwlxml.zip";

                /*
                 * Let's see if I can make this relatively uncaring about the
                 * order of the parameters. We'll expect the param be of the form
                 * 
                 *      paramName:paramValue
                 *      
                 * with the colon being the separator. This will probably break dismally
                 * if the humans don't follow this convention. Sigh.
                 */
                if (args.Length < 1)
                {
                    Console.WriteLine("No input argments passed to program. Using defaults");
                }
                else
                {
                    Console.WriteLine(string.Format("{0} arguments passed:", args.Length));
                    for (int i = 0; i < args.Length; i++)
                    {
                        Console.WriteLine(string.Format("   {0}", args[i]));
                    }
                }
                /*
                 * Let's see if the humans passed in any arguments.
                 */
                for (int i = 0; i < args.Length; i++)
                {
                    string[] thisArg = args[i].Split(':');

                    if (args[i].ToLower().StartsWith("filename"))
                    {
                        zipFileName = thisArg[1];
                    }
                    if (args[i].ToLower().StartsWith("filepath"))
                    {
                        filePath = thisArg[1];
                    }
                    if (args[i].ToLower().StartsWith("ftpuserid"))
                    {
                        ftpUserID = thisArg[1];
                    }
                    if (args[i].ToLower().StartsWith("ftppassword"))
                    {
                        ftpPassword = thisArg[1];
                    }
                    if (args[i].ToLower().StartsWith("ftpserver"))
                    {
                        ftpServerIP = thisArg[1];
                    }
                    if (args[i].ToLower().StartsWith("localzipfile"))
                    {
                        localZipFileName = thisArg[1];
                    }
                }

                /*
                 * Now we're going to concoct the checksum file name based on
                 * the name of the zip file. This is convoluted (and bugs me)
                 * because the Accuity guys are doing something weird with the
                 * mlgwxml file name - the zip file is in all uppercase and the
                 * md5 checksum file name is in lower case. Go figure. I want to
                 * make everything lower case on our end. Stay tuned to see if that
                 * ends up screwing us up or not.
                 * 
                 * However, the humans could have explicitly supplied a local zip
                 * file name. If they did, we'll use that ...
                 */
                if (localZipFileName.Length == 0)
                {
                    localZipFileName = zipFileName.ToLower();
                }
                md5FileName = localZipFileName.Substring(0, localZipFileName.IndexOf(".") + 1)
                    + "md5";
                localMd5FileName = md5FileName;

                /*
                 * All right, let's write out our values. This will end up in the Appworx
                 * log file so that might come in handy some day.
                 */
                Console.WriteLine("Using these values:");
                Console.WriteLine(string.Format("   filename:{0}", zipFileName));
                Console.WriteLine(string.Format("   localzipfilename:{0}", localZipFileName));
                Console.WriteLine(string.Format("   filepath:{0}", filePath));
                Console.WriteLine(string.Format("   ftpuserid:{0}", ftpUserID));
                Console.WriteLine(string.Format("   ftppassword:{0}", ftpPassword));
                Console.WriteLine(string.Format("   ftpserver:{0}", ftpServerIP));


                /*
                 * Get the checksum file from the Accuity site.
                 */
                ofacFtp.downloadFile(filePath,
                          md5FileName,
                          localMd5FileName,
                          ftpUserID,
                          ftpPassword,
                          ftpServerIP);

                /*
                 * Let's see if the checksum file is the same as the one
                 * we got on the last run. If so, there's no need to pull
                 * down the big data file itself. If the checksums are
                 * different, go get the big zip file.
                 */
                bool newOfacFile = ofacFtp.compareMd5(filePath, localMd5FileName);

                if (newOfacFile)
                {
                    /*
                     * Ok, they've got a new file for us on the Accuity site.
                     * Go get it.
                     */
                    Console.WriteLine(string.Format("There's a new {0} file", zipFileName));

                    ofacFtp.downloadFile(filePath,
                                zipFileName,
                                localZipFileName,
                                ftpUserID,
                                ftpPassword,
                                ftpServerIP);

                    /*
                     * This is so greasy ... since we're pulling down the
                     * new mlgwlxml file, let's also (re)pull the md5 file
                     * and save it as the current md5 file. It would probably
                     * be a few milliseconds faster to have the code rename
                     * or copy the new md5 file that we just downloaded, but 
                     * what the heck, the file's small and we're here right now.
                     */
                    localMd5FileName = "current_" + localMd5FileName;
                    ofacFtp.downloadFile(filePath,
                               md5FileName,
                               localMd5FileName,
                               ftpUserID,
                               ftpPassword,
                               ftpServerIP);
                }
                else
                {
                    Console.WriteLine(string.Format("There's NO new {0} file", zipFileName));
                }
                ofacFtp.emailEverybody(newOfacFile, localZipFileName);
            }
            catch (Exception ex)
            {
                allsWellThatEndsWell = false;
                Console.WriteLine("");
                Console.WriteLine(string.Format("OFAC Ftp Error: {0}", ex.Message.ToString()));
            }
            if (allsWellThatEndsWell)
            {
                Console.WriteLine(string.Format("OFAC Ftp successfull: {0}", DateTime.Now));
            }
            Console.WriteLine("");
        }


        private void downloadFile(string filePath,
            string fileName,
            string localFileName,
            string ftpUserID,
            string ftpPassword,
            string ftpServerIP)
        {
            FtpWebRequest reqFTP;
            try
            {
                FileStream outputStream = new FileStream(filePath + "\\" +
                   localFileName, FileMode.Create);

                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://"
                   + ftpServerIP + "/" + fileName));
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                Stream ftpStream = response.GetResponseStream();
                long cl = response.ContentLength;
                int bufferSize = 4096;
                int readCount;
                byte[] buffer = new byte[bufferSize];

                readCount = ftpStream.Read(buffer, 0, bufferSize);
                while (readCount > 0)
                {
                    outputStream.Write(buffer, 0, readCount);
                    readCount = ftpStream.Read(buffer, 0, bufferSize);
                }

                ftpStream.Close();
                outputStream.Close();
                response.Close();
            }
            catch (Exception ftpException)
            {
                /*
                 * Let the caller catch this guy.
                 */
                Exception ftp = new Exception(ftpException.Message + " File:" + fileName);
                //ftpException.Message = ftpException.Message + " File:" + fileName;
                throw (ftp);
            }
            return;
        }

        private bool compareMd5(string filePath,
            string newMd5fileName)
        {
            FileStream currentMd5;
            FileStream newMd5;
            string fileName = string.Format("current_{0}", newMd5fileName);
            try
            {
                currentMd5 = new FileStream(filePath + "\\" +
                fileName, FileMode.Open);
            }
            catch
            {
                /*
                 * Couldn't open the old md5 file. We could have
                 * a problem, Houston, or we could just be testing ...
                 * What to do? Let's pretend all's well and go pull 
                 * down the zip file.
                 */
                return true;
            }

            newMd5 = new FileStream(filePath + "\\" +
            newMd5fileName, FileMode.Open);

            int bufLength = 64;
            byte[] currentChecksum = new byte[bufLength];

            System.Text.ASCIIEncoding encoder = new System.Text.ASCIIEncoding();

            int md5Bytes = currentMd5.Read(currentChecksum, 0, bufLength);
            string md5Checksum = encoder.GetString(currentChecksum, 0, md5Bytes);

            md5Bytes = newMd5.Read(currentChecksum, 0, bufLength);
            string newMd5Checksum = encoder.GetString(currentChecksum, 0, md5Bytes);

            newMd5.Close();
            currentMd5.Close();

            /*
             * So, finally, now let's see if the checksums match.
             */
            if (newMd5Checksum.Equals(md5Checksum))
            {
                return false;   // as in NOT a new file on their site ...
            }
            else
            {
                return true;
            }
        }

        private void emailEverybody(bool newFile, string fileName)
        {
            System.Diagnostics.Debug.WriteLine("OFAC Ftp:  emailEverybody - enter");
            Console.WriteLine("Sending Email");

            string emailAddress;
            emailAddress = "OFAC_FTP@fhlbsf.com";
            //emailAddress = "richardj@fhlbsf.com";
            /*
             * Let's allow the humans to pass in multiple email addresses. We'll ask them to separate
             * the individual address with a semi-colon. If they do that, all should work. Ha!
             */
            string[] emailAddresses = emailAddress.Split(';');
            MailAddress from = new MailAddress("OFAC_FTP@fhlbsf.com");
            MailAddress to = new MailAddress(emailAddresses[0].ToString().Trim());
            MailMessage message = new MailMessage(from, to);

            for (int i = 1; i < emailAddresses.Length; i++)
            {
                /*
                 * Watch out for any blank address strings. You'll get this if there 
                 * are 'extra' (trailing) semi-colons in the email string.
                 */
                if (emailAddresses[i].Length > 0)
                {
                    MailAddress adr = new MailAddress(emailAddresses[i].ToString().Trim());
                    message.To.Add(adr);
                }
            }

            /*
             * Ok, now we've got all the info ... format the msg and send
             * it on it's merry way.
             */
            StringBuilder body = new StringBuilder();
            fileName = fileName.Substring(0, fileName.IndexOf("."));
            if (newFile)
            {
                Console.WriteLine("   Email - changed/new file");
                message.Subject = message.Subject = string.Format("OFAC FTP - Changes for {0}", fileName);
                body.Append(string.Format("New {0} Database as of {1}", fileName, DateTime.Now));
            }
            else
            {
                Console.WriteLine("   Email - no changes");
                message.Subject = string.Format("OFAC FTP - No Changes for {0}", fileName);
                body.Append(string.Format("existing {0} Database current {1}", fileName, DateTime.Now));
            }

            message.Body = body.ToString();
            message.Priority = MailPriority.High;

            SmtpClient aSmtpClient = new SmtpClient();
            try
            {
                aSmtpClient.Send(message);
            }
            catch (Exception mailException)
            {
                System.Console.WriteLine(mailException.Message);
                throw (mailException);
            }
        }

        private void help()
        {
            Console.WriteLine("OfacFtp - if you run this program with no input parameters\n" +
                              "it will by default download the MLGWLXML.ZIP file from the\n" +
                              "Accuity server\n");

            Console.WriteLine("You can override this behavior by entering parameters for a\n" +
                              "variety of things - server name, password, filename, etc");

            Console.WriteLine("The input parameters are tag-driven and the order is unimportant.\n" +
                              "The tag is not case-sensitive, but the *value* of the tag is important.\n" +
                              "You can mix and match parameters as you wish. The list of parameters is this:\n");

            Console.WriteLine(string.Format("   filename:<the file name on the Accuity site>"));
            Console.WriteLine(string.Format("   localzipfilename:<the local file name for the file downloaded>"));
            Console.WriteLine(string.Format("   filepath:<the path where the file will be stored>"));
            Console.WriteLine(string.Format("   ftpuserid:<Accuity ftp user id>"));
            Console.WriteLine(string.Format("   ftppassword:<Accuity ftp password>"));
            Console.WriteLine(string.Format("   ftpserver:<Accuity ftp server url>"));
            Console.WriteLine("\n");
        }
    }
}
